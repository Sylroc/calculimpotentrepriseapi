CREATE TABLE IF NOT EXISTS compagnies (
	siret VARCHAR(40) NOT NULL,
	type  VARCHAR(40) NOT NULL,
	denomination  VARCHAR(40) NOT NULL,
	adresse VARCHAR(200),
	ca DOUBLE,
	PRIMARY KEY (siret)
);
	