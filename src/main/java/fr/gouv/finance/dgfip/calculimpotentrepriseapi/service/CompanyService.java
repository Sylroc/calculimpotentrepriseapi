package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service;

import java.util.Optional;

import org.springframework.stereotype.Service;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;

@Service
public interface CompanyService {

	/**
	 * Find a compagny from a Siret
	 * 
	 * @param siret
	 * @return
	 */
	public Optional<Compagny> findSiret(String siret);

	/**
	 * Save a CompagnySAS
	 * 
	 * @param compagnySAS
	 * @return
	 */
	public Optional<Compagny> saveSAS(Compagny compagnySAS);

	/**
	 * Save a CompagnyAuto
	 * 
	 * @param mockCompagny
	 * @return
	 */
	public Optional<Compagny> saveAuto(Compagny mockCompagny);

}
