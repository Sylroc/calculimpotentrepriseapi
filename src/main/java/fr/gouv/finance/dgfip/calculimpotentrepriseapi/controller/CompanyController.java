package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;

@RestController
/**
 * 
 * Controller which create, find compagnies from WebAPI to H2Database
 *
 */
public interface CompanyController {

	/**
	 * Return Compagny from Siret Number
	 * 
	 * @param siret Pass the siret Number
	 * @return The Json of a compagny
	 */
	public ResponseEntity<?> getCompagny(@PathVariable String siret);

	/**
	 * Add a compagny of type SAS to the database
	 * 
	 * @param compagnySAS. Need siret, denomination, adresse, ca (optionnal)
	 * @return The Json of a compagny SAS
	 */
	public ResponseEntity<Compagny> postCompagnySAS(@RequestBody CompagnySAS compagnySAS);

	/**
	 * Add a compagny of type AutoEntreprise to the database
	 * 
	 * @param compagnyAuto. Need siret, denomination, ca (optionnal)
	 * @return The Json of a compagny AutoEntreprise
	 */
	public ResponseEntity<Compagny> postCompagnyAuto(@RequestBody CompagnyAuto compagnyAuto);
}
