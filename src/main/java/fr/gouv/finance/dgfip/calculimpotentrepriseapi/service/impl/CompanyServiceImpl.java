package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.impl;

import java.util.Optional;

import org.springframework.stereotype.Service;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository.CompagnyRepository;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {

	private final CompagnyRepository compagnyRepository;

	public CompanyServiceImpl(CompagnyRepository compagnyRepository) {
		this.compagnyRepository = compagnyRepository;
	}

	public Optional<Compagny> findSiret(String siret) {
		return compagnyRepository.find(siret);
	}

	@Override
	public Optional<Compagny> saveSAS(Compagny compagnySAS) {
		if (checkSAS(compagnySAS)) {
			return Optional.of(compagnyRepository.saveAutoSAS(compagnySAS));
		}
		return Optional.empty();
	}

	@Override
	public Optional<Compagny> saveAuto(Compagny compagnyAuto) {
		if (checkAutoEntrprise(compagnyAuto)) {
			return Optional.of(compagnyRepository.saveAutoEntreprise(compagnyAuto));
		}
		return Optional.empty();
	}

	private boolean checkSAS(Compagny compagny) {

		return compagny.getSiret() != null && compagny.getDenomination() != null
				&& ((CompagnySAS) compagny).getAdresse() != null;

	}

	private boolean checkAutoEntrprise(Compagny compagny) {
		return compagny.getSiret() != null && compagny.getDenomination() != null;

	}
}
