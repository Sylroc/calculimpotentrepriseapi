package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service;

import java.util.Optional;

public interface ImpotService {

	
	/**
	 * Find compagny from siret and calculate taxe 
	 * @param siret
	 * @return
	 */
	public Optional<Double> findSiretAndCalulate(String siret);

}
