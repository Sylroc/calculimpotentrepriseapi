package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller.CompanyController;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.CompanyService;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2

public class CompanyControllerImp implements CompanyController {

	private final CompanyService companyService;

	public CompanyControllerImp(CompanyService companyService) {
		this.companyService = companyService;
	}

	@GetMapping("/compagny/{siret}")
	public ResponseEntity<?> getCompagny(@PathVariable String siret) {
		log.info("Ask for Compagny Siret " + siret);
		Optional<Compagny> compOptional = companyService.findSiret(siret);
		if (compOptional.isPresent()) {
			try {
				log.info("Return compagny " + compOptional.get() + " for the Siret " + siret);
				return ResponseEntity.ok().location(new URI("/compagny/" + compOptional.get().getSiret()))
						.body(compOptional.get());
			} catch (URISyntaxException e) {
				log.info("Return INTERNAL SERVER ERROR for the Siret " + siret);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			log.info("Return NOT FOUND for the Siret " + siret);
			return ResponseEntity.notFound().build();
		}

	}

	@PostMapping("/compagnySAS")
	public ResponseEntity<Compagny> postCompagnySAS(@RequestBody CompagnySAS compagnySAS) {
		log.info("Create new compagny :" + compagnySAS);
		Optional<Compagny> compOptional = companyService.saveSAS(compagnySAS);
		if (compOptional.isPresent()) {
			try {
				log.info("Return new compagny " + compOptional.get());
				return ResponseEntity.created(new URI("/compagny/" + compOptional.get().getSiret()))
						.body(compOptional.get());
			} catch (URISyntaxException e) {
				log.info("Return INTERNAL SERVER ERROR for the compagny " + compOptional.get());
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			log.info("Return Compagny was not create " + compagnySAS);
			return ResponseEntity.notFound().build();
		}
	}

	@PostMapping("/compagnyAuto")
	public ResponseEntity<Compagny> postCompagnyAuto(@RequestBody CompagnyAuto compagnyAuto) {
		log.info("Create new compagny :" + compagnyAuto);
		Optional<Compagny> compOptional = companyService.saveAuto(compagnyAuto);
		if (compOptional.isPresent()) {
			try {
				log.info("Return new compagny " + compOptional.get());
				return ResponseEntity.created(new URI("/compagny/" + compOptional.get().getSiret()))
						.body(compOptional.get());
			} catch (URISyntaxException e) {
				log.info("Return INTERNAL SERVER ERROR for the compagny " + compOptional.get());
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			log.info("Return Compagny was not create " + compagnyAuto);
			return ResponseEntity.notFound().build();
		}
	}
}
