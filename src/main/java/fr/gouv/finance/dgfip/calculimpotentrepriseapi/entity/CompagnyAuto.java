package fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CompagnyAuto extends Compagny {

	private static final double TAUXIMPOSITION = 25.0;

	public CompagnyAuto(String siret, String denomination, long ca) {
		super(siret, denomination, ca);
	}
	
	
	@Override
	public double calcuImpot() {
		double impot = getCa() * (TAUXIMPOSITION / 100);

		BigDecimal bd = new BigDecimal(impot).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
