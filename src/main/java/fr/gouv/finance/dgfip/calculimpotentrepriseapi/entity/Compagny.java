package fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
public abstract class Compagny {

	@Getter
	private String siret;
	@Getter
	@Setter
	private String type;
	@Getter
	private String denomination;
	@Getter
	private double ca;

	public Compagny(String siret, String denomination, double ca) {
		this.siret = siret;
		this.denomination = denomination;
		this.ca = ca;
	}

	public Compagny(String siret, String type, String denomination, long ca) {
		this.siret = siret;
		this.denomination = denomination;
		this.ca = ca;
		this.type = type;
	}

	public abstract double calcuImpot();
}
