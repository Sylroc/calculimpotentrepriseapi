package fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.Getter;

public class CompagnySAS extends Compagny {

	private static final double TAUXIMPOSITION = 33.0;

	@Getter
	private String adresse;

	public CompagnySAS(String siret, String denomination, String adresse, long ca) {
		super(siret, denomination, ca);
		this.adresse = adresse;
	}

	@Override
	public double calcuImpot() {
		double impot = getCa() * (TAUXIMPOSITION / 100.0);

		BigDecimal bd = new BigDecimal(impot).setScale(2, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
