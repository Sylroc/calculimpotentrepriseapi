package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
/**
 * 
 * Controller which calculate taxes
 *
 */
public interface ImpotController {

	/**
	 * Return Compagny from Siret Number
	 * 
	 * @param siret Pass the siret Number
	 * @return The amount of taxes pay by the compagny
	 */
	@GetMapping("/compagny/{siret}")
	public ResponseEntity<?> getCompagny(@PathVariable String siret);

}
