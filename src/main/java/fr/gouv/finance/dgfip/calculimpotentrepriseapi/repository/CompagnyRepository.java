package fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository;

import java.util.Optional;

import org.springframework.stereotype.Repository;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;

@Repository
public interface CompagnyRepository {

	/**
	 * Find a compagny from a siret number in Database
	 * 
	 * @param siret
	 * @return Optional of COmpagny
	 */
	Optional<Compagny> find(String siret);

	/**
	 * Save a compagny of AutoEntreprise in Database
	 * 
	 * @param compagny
	 * @return compagny
	 */
	Compagny saveAutoEntreprise(Compagny compagny);

	/**
	 * Save a compagny of SAS in Database
	 * 
	 * @param compagny
	 * @return compagny
	 */
	Compagny saveAutoSAS(Compagny compagny);

}
