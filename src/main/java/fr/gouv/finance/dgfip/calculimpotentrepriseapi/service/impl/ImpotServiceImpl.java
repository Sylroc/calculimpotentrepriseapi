package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.CompanyService;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.ImpotService;

@Service
public class ImpotServiceImpl implements ImpotService {

	@Autowired
	private final CompanyService companyService;

	public ImpotServiceImpl(CompanyService companyService) {
		this.companyService = companyService;
	}

	@Override
	public Optional<Double> findSiretAndCalulate(String siret) {
		Optional<Compagny> compagny = companyService.findSiret(siret);
		if (compagny.isPresent()) {
			return Optional.of(compagny.get().calcuImpot());
		}

		return Optional.empty();
	}

}
