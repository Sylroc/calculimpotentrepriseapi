package fr.gouv.finance.dgfip.calculimpotentrepriseapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalculImpotEntrepriseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CalculImpotEntrepriseApiApplication.class, args);
	}

}
