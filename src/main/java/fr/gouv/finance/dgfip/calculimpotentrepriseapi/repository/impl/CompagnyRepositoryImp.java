package fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.sql.DataSource;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository.CompagnyRepository;
import lombok.extern.log4j.Log4j2;

@Repository
@Log4j2
public class CompagnyRepositoryImp implements CompagnyRepository {

	private final JdbcTemplate jdbcTemplate;
	private final SimpleJdbcInsert simpleJdbcInsert;

	public CompagnyRepositoryImp(JdbcTemplate jdbcTemplate, DataSource dataSource) {
		this.jdbcTemplate = jdbcTemplate;
		this.simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("Compagnies");
	}

	@Override
	public Optional<Compagny> find(String siret) {
		try {
			Compagny compagny = jdbcTemplate.queryForObject("SELECT * FROM COMPAGNIES where siret = ?",
					new Object[] { siret }, (rs, rowNum) -> {
						Compagny c = null;
						if (rs.getString("type").equalsIgnoreCase("autoentreprises")) {
							c = new CompagnyAuto(rs.getString("siret"), rs.getString("denomination"), rs.getInt("ca"));
							c.setType("autoentreprises");
						}
						if (rs.getString("type").equalsIgnoreCase("sas")) {
							c = new CompagnySAS(rs.getString("siret"), rs.getString("denomination"),
									rs.getString("adresse"), rs.getInt("ca"));
							c.setType("sas");
						}
						return c;
					});
			return Optional.of(compagny);
		} catch (EmptyResultDataAccessException e) {
			return Optional.empty();
		}
	}

	@Override
	public Compagny saveAutoEntreprise(Compagny compagny) {
		Map<String, Object> map = new HashMap<>();
		map.put("SIRET", compagny.getSiret());
		map.put("TYPE", "autoentreprises");
		map.put("DENOMINATION", compagny.getDenomination());
		map.put("CA", compagny.getCa());
		simpleJdbcInsert.execute(map);
		log.info("Insert compagny" + compagny);
		compagny.setType("autoentreprises");
		return compagny;
	}

	@Override
	public Compagny saveAutoSAS(Compagny compagny) {
		Map<String, Object> map = new HashMap<>();
		map.put("siret", compagny.getSiret());
		map.put("type", "sas");
		map.put("denomination", compagny.getDenomination());
		map.put("adresse", ((CompagnySAS) compagny).getAdresse());
		map.put("ca", compagny.getCa());
		simpleJdbcInsert.execute(map);
		log.info("Insert compagny" + compagny);
		compagny.setType("sas");
		return compagny;
	}

}
