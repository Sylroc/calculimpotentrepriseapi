package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller.impl;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller.ImpotController;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.ImpotService;
import lombok.extern.log4j.Log4j2;

@RestController
@Log4j2
/**
 * 
 * Controller which calculate taxes
 *
 */
public class ImpotControllerImp implements ImpotController {

	private final ImpotService impotService;

	public ImpotControllerImp(ImpotService impotService) {
		this.impotService = impotService;
	}

	@GetMapping("/impot/{siret}")
	public ResponseEntity<?> getCompagny(@PathVariable String siret) {
		log.info("Ask for Compagny Siret to pay Impot " + siret);
		Optional<Double> impot = impotService.findSiretAndCalulate(siret);
		if (impot.isPresent()) {
			try {
				log.info("The Siret " + siret + "should pay " + impot.get());
				return ResponseEntity.ok().location(new URI("/impot/" + siret)).body(impot.get());
			} catch (URISyntaxException e) {
				log.info("Return INTERNAL SERVER ERROR for the Siret " + siret);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
			}
		} else {
			log.info("Return NOT FOUND for the Siret " + siret);
			return ResponseEntity.notFound().build();
		}

	}
}
