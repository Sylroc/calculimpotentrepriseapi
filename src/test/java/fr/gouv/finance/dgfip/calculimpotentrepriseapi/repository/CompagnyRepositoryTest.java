package fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Optional;

import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.junit5.DBUnitExtension;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.CalculImpotEntrepriseApiApplication;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;

@ExtendWith({ DBUnitExtension.class, SpringExtension.class })
@AutoConfigureMockMvc
@SpringBootTest(classes = CalculImpotEntrepriseApiApplication.class)
class CompagnyRepositoryTest {

	@Autowired
	private DataSource dataSource;

	@Autowired
	private CompagnyRepository compagnyRepository;

	public ConnectionHolder getConnectionHolder() {
		return () -> dataSource.getConnection();
	}

	@Test
	@DataSet("compagnies.yml")
	void getCompagnyAutoFromDB() {
		Optional<Compagny> compagny = compagnyRepository.find("123456789012345");
		assertNotNull(compagny.get());
		assertEquals("123456789012345", compagny.get().getSiret());
		assertEquals("My compagny", compagny.get().getDenomination());
		assertEquals("autoentreprises", compagny.get().getType());
		assertEquals(1000000.0, compagny.get().getCa());
	}

	@Test
	@DataSet("compagnies.yml")
	void getCompagnySASFromDB() {
		Optional<Compagny> compagny = compagnyRepository.find("234567890123456");
		assertNotNull(compagny.get());
		assertEquals("234567890123456", compagny.get().getSiret());
		assertEquals("My compagny", compagny.get().getDenomination());
		assertEquals("sas", compagny.get().getType());
		assertEquals("Somewhere over the rainbow", ((CompagnySAS) compagny.get()).getAdresse());
		assertEquals(2000000.0, compagny.get().getCa());
	}

	@Test
	@DataSet("compagnies.yml")
	void saveCompagnyAutoFromDB() {

		Compagny compagny = new CompagnyAuto("1111111", "TheOther", 2000);
		Compagny compagnyReturnSave = compagnyRepository.saveAutoEntreprise(compagny);
		assertEquals("autoentreprises", compagnyReturnSave.getType());		
		
		Optional<Compagny> compagnyReturnFind = compagnyRepository.find("1111111");		
		assertEquals("1111111", compagnyReturnFind.get().getSiret());
		assertEquals("TheOther", compagnyReturnFind.get().getDenomination());
		assertEquals("autoentreprises", compagnyReturnFind.get().getType());
		assertEquals(2000.0, compagnyReturnFind.get().getCa());
	}

	@Test
	@DataSet("compagnies.yml")
	void saveCompagnySASFromDB() {

		Compagny compagny = new CompagnySAS("222222", "TheOther2", "Here", 3000);
		Compagny compagnyReturnSave = compagnyRepository.saveAutoSAS(compagny);
		assertEquals("sas", compagnyReturnSave.getType());		
		
		Optional<Compagny> compagnyReturnFind = compagnyRepository.find("222222");		
		assertEquals("222222", compagnyReturnFind.get().getSiret());
		assertEquals("TheOther2", compagnyReturnFind.get().getDenomination());
		assertEquals("sas", compagnyReturnFind.get().getType());
		assertEquals(3000, compagnyReturnFind.get().getCa());
	}
}
