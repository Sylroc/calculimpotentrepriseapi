package fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository;

import static org.junit.jupiter.api.Assertions.*;

import javax.activation.DataSource;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@Profile("test")
class CompagnyRepositoryConfiguration {

	@Primary
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl("jdbc:h2:mem:db);DB_CLOSE_DELAY=-1");
		return (DataSource) dataSource;
	}
}
