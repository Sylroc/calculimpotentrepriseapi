package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.CalculImpotEntrepriseApiApplication;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.CompanyService;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.ImpotService;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = CalculImpotEntrepriseApiApplication.class)
class ImpotServiceTest {

	@Autowired
	private ImpotService impotService;

	@MockBean
	private CompanyService compagnyService;

	@Test
	void findByIdSuccessAutoEntreprise() {
		Compagny mockCompagny = new CompagnyAuto("1234567890123",  "MyCompagnyAuto", 1000000);
		doReturn(Optional.of(mockCompagny)).when(compagnyService).findSiret("1234567890123");
		Optional<Double> impot = impotService.findSiretAndCalulate("1234567890123");
		assertTrue(impot.isPresent(), "Impot not found");
		assertEquals(250000.0,impot.get().doubleValue(), "Impot is not the same");
	}

	@Test
	void findByIdSuccessSAS() {
		Compagny mockCompagny = new CompagnySAS("2345678901234",  "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		doReturn(Optional.of(mockCompagny)).when(compagnyService).findSiret("2345678901234");
		Optional<Double> impot = impotService.findSiretAndCalulate("2345678901234");
		assertTrue(impot.isPresent(), "Impot not found");
		assertEquals(660000.0, impot.get(), "Impot is not the same");
	}

	@Test
	void findByIdFail() {
		doReturn(Optional.empty()).when(compagnyService).findSiret("2345678901234");
		Optional<Double> compagnyReturn = impotService.findSiretAndCalulate("2345678901234");
		assertFalse(compagnyReturn.isPresent(), "Impot found");
	}

}
