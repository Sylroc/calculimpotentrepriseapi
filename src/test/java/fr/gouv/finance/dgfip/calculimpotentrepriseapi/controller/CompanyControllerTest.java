package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.CalculImpotEntrepriseApiApplication;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.impl.CompanyServiceImpl;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = CalculImpotEntrepriseApiApplication.class)
class CompanyControllerTest {

	@MockBean
	private CompanyServiceImpl compagnyService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getAutoEntreprisesCompagnyBySiretFound() throws Exception {
		Compagny mockCompagny = new CompagnyAuto("1234567890123", "MyCompagnyAuto", 1000000);
		mockCompagny.setType("autoentreprises");
		doReturn(Optional.of(mockCompagny)).when(compagnyService).findSiret("1234567890123");
		mockMvc.perform(get("/compagny/{siret}", "1234567890123")).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.LOCATION, "/compagny/1234567890123"))
				.andExpect(jsonPath("$.siret", is("1234567890123")))
				.andExpect(jsonPath("$.type", is("autoentreprises")))
				.andExpect(jsonPath("$.denomination", is("MyCompagnyAuto"))).andExpect(jsonPath("$.ca", is(1000000.0)));

	}

	@Test
	void getSASCompagnyBySiretFound() throws Exception {
		Compagny mockCompagny = new CompagnySAS("2345678901234", "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		mockCompagny.setType("SAS");
		doReturn(Optional.of(mockCompagny)).when(compagnyService).findSiret("2345678901234");
		mockMvc.perform(get("/compagny/{siret}", "2345678901234")).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.LOCATION, "/compagny/2345678901234"))
				.andExpect(jsonPath("$.siret", is("2345678901234"))).andExpect(jsonPath("$.type", is("SAS")))
				.andExpect(jsonPath("$.denomination", is("MyCompagnySAS")))
				.andExpect(jsonPath("$.adresse", is("Somewhere Over the Rainbow")))
				.andExpect(jsonPath("$.ca", is(2000000.0)));

	}

	@Test
	void getCompagnyBySiretNotFound() throws Exception {
		doReturn(Optional.empty()).when(compagnyService).findSiret("3456789012345");
		mockMvc.perform(get("/compagny/{siret}", "3456789012345")).andExpect(status().isNotFound());
	}

	@Test
	void postAutoEntreprisesCompagny() throws Exception {

		Compagny postCompagny = new CompagnyAuto("45678901234567", "MyCompagnyAuto", 1000000);
		Compagny mockCompagny = new CompagnyAuto("45678901234567", "MyCompagnyAuto", 1000000);
		mockCompagny.setType("autoentreprises");
		Mockito.when(compagnyService.saveAuto(Mockito.any(Compagny.class))).thenReturn(Optional.of(mockCompagny));

		mockMvc.perform(
				post("/compagnyAuto").contentType(MediaType.APPLICATION_JSON).content(asJsonString((postCompagny))))
				.andExpect(status().isCreated())
				.andExpect(header().string(HttpHeaders.LOCATION, "/compagny/45678901234567"))
				.andExpect(jsonPath("$.siret", is("45678901234567")))
				.andExpect(jsonPath("$.type", is("autoentreprises")))
				.andExpect(jsonPath("$.denomination", is("MyCompagnyAuto"))).andExpect(jsonPath("$.ca", is(1000000.0)));
	}

	@Test
	void postSASCompagny() throws Exception {
		Compagny postCompagny = new CompagnySAS("2345678901234", "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		Compagny mockCompagny = new CompagnySAS("2345678901234", "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		mockCompagny.setType("SAS");
		Mockito.when(compagnyService.saveSAS(Mockito.any(CompagnySAS.class))).thenReturn(Optional.of(mockCompagny));
		mockMvc.perform(
				post("/compagnySAS").contentType(MediaType.APPLICATION_JSON).content(asJsonString((postCompagny))))
				.andExpect(status().isCreated())
				.andExpect(header().string(HttpHeaders.LOCATION, "/compagny/2345678901234"))
				.andExpect(jsonPath("$.siret", is("2345678901234"))).andExpect(jsonPath("$.type", is("SAS")))
				.andExpect(jsonPath("$.denomination", is("MyCompagnySAS")))
				.andExpect(jsonPath("$.adresse", is("Somewhere Over the Rainbow")))
				.andExpect(jsonPath("$.ca", is(2000000.0)));
	}

	// Convert Object as JSon String
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
