package fr.gouv.finance.dgfip.calculimpotentrepriseapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.CalculImpotEntrepriseApiApplication;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.Compagny;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnyAuto;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.entity.CompagnySAS;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.repository.CompagnyRepository;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.CompanyService;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = CalculImpotEntrepriseApiApplication.class)
class CompanyServiceTest {

	@Autowired
	private CompanyService compagnyService;

	@MockBean
	private CompagnyRepository compagnyRepository;

	@Test
	void findByIdSuccessAutoEntreprise() {
		Compagny mockCompagny = new CompagnyAuto("1234567890123", "MyCompagnyAuto", 1000000);
		doReturn(Optional.of(mockCompagny)).when(compagnyRepository).find("1234567890123");
		Optional<Compagny> compagnyReturn = compagnyService.findSiret("1234567890123");
		assertTrue(compagnyReturn.isPresent(), "Compagny not found");
		assertSame(compagnyReturn.get(), mockCompagny, "Compagny is the same");
	}

	@Test
	void findByIdSuccessSAS() {
		Compagny mockCompagny = new CompagnySAS("2345678901234", "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		doReturn(Optional.of(mockCompagny)).when(compagnyRepository).find("2345678901234");
		Optional<Compagny> compagnyReturn = compagnyService.findSiret("2345678901234");
		assertTrue(compagnyReturn.isPresent(), "Compagny not found");
		assertSame(compagnyReturn.get(), mockCompagny, "Compagny is the same");
	}

	@Test
	void findByIdFail() {
		doReturn(Optional.empty()).when(compagnyRepository).find("2345678901234");
		Optional<Compagny> compagnyReturn = compagnyService.findSiret("2345678901234");
		assertFalse(compagnyReturn.isPresent(), "Compagny found");
	}

	@Test
	void saveAutoEntreprise() {
		Compagny mockCompagny = new CompagnyAuto("1234567890123", "MyCompagnyAuto", 1000000);
		doReturn(mockCompagny).when(compagnyRepository).saveAutoEntreprise(mockCompagny);
		Optional<Compagny> compagnyReturn = compagnyService.saveAuto(mockCompagny);

		assertNotNull(compagnyReturn, "Compagny should not be null");
		assertEquals(compagnyReturn.get(), mockCompagny, "Compagnies are the same");
	}

	@Test
	void saveSAS() {
		Compagny mockCompagny = new CompagnySAS("2345678901234", "MyCompagnySAS", "Somewhere Over the Rainbow",
				2000000);
		doReturn(mockCompagny).when(compagnyRepository).saveAutoSAS(mockCompagny);
		Optional<Compagny> compagnyReturn = compagnyService.saveSAS(mockCompagny);

		assertNotNull(compagnyReturn, "Compagny should not be null");
		assertEquals(compagnyReturn.get(), mockCompagny, "Compagnies are the same");
	}

}
