package fr.gouv.finance.dgfip.calculimpotentrepriseapi.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import fr.gouv.finance.dgfip.calculimpotentrepriseapi.CalculImpotEntrepriseApiApplication;
import fr.gouv.finance.dgfip.calculimpotentrepriseapi.service.ImpotService;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = CalculImpotEntrepriseApiApplication.class)
class ImpotControllerTest {

	@MockBean
	private ImpotService impotService;

	@Autowired
	private MockMvc mockMvc;

	@Test
	void getImpotBySiretFound() throws Exception {
		double mockimpot = 370000;
		doReturn(Optional.of(mockimpot)).when(impotService).findSiretAndCalulate("1234567890123");
		MvcResult result = mockMvc.perform(get("/impot/{siret}", "1234567890123")).andExpect(status().isOk())
				.andExpect(header().string(HttpHeaders.LOCATION, "/impot/1234567890123")).andReturn();
		assertEquals(result.getResponse().getContentAsString(), "370000.0", "Impots are not the same");

	}

	@Test
	void getCompagnyBySiretNotFound() throws Exception {
		doReturn(Optional.empty()).when(impotService).findSiretAndCalulate("3456789012345");
		mockMvc.perform(get("/impot/{siret}", "3456789012345")).andExpect(status().isNotFound());
	}
}
